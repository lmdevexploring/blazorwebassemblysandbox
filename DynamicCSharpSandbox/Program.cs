﻿
using DynamicCSharpSandbox;

Console.WriteLine("Hello, Dynamic C#!");

Calculator calculator = new Calculator();

var result = calculator.GetType().InvokeMember("Add",
    System.Reflection.BindingFlags.InvokeMethod,
    null,
    calculator,
    new object[] { 10, 20 });

Console.WriteLine($"Using Reflection here :: Sum = {result}");


dynamic dynCalculator = new Calculator();
var dynResult = dynCalculator.Add(10, 20);
Console.WriteLine($"Using Dynamic C# here :: Sum = {dynResult}");