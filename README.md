# My Own Blazor's WebAssembly (WASM) Sandbox
This is my Blazor WASM Sandbox. And a little dive into dynamic c#.

<ol>
	<li>.Net 6</li>
	<li>Entity Framework Core 6</li>
	<li>Syncfusion Components</li>
	<li>Dive into Dynamic C#</li>
		
</ol>

You can check this project's API Sandbox in my [Postman's API Slashing](https://www.postman.com/apislasherteam/workspace/public-dev-exploring/collection/11816054-f89dcdcc-448b-490e-b8e4-18863c8b7aa0).


# Blazor WebAssembly (WASM) Resources

[Microsoft Blazor](https://dotnet.microsoft.com/en-us/apps/aspnet/web-apps/blazor)

[Web development with Blazor: Kudvenkat's Tutorial](https://www.youtube.com/playlist?list=PL6n9fhu94yhXXmhl1U4_oHZS5nhaabpPN)

[Syncfusion Blazor Components](https://www.syncfusion.com/blazor-components)

---

# Dynamic C# Resources

[Dynamic C#](https://docs.microsoft.com/en-us/dotnet/api/system.dynamic?view=net-6.0)
