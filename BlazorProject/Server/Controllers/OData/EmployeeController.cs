﻿using BlazorProject.Server.Models;
using BlazorProject.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorProject.Server.Controllers.OData
{
    [ApiController]
    [Route("[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class EmployeesODataController : ControllerBase
    {
        private readonly IEmployeeRepository employeeRepository;
        private readonly AppDbContext appDbContext;

        public EmployeesODataController(IEmployeeRepository employeeRepository, AppDbContext appDbContext)
        {
            this.employeeRepository = employeeRepository;
            this.appDbContext = appDbContext;
        }

        [HttpGet]
        [EnableQuery(PageSize = 15)]
        public IQueryable<Employee> Get()
        {
            return appDbContext.Employees;
        }

    }
}