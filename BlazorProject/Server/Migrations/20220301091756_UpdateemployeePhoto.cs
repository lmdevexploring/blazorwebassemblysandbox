﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BlazorProject.Server.Migrations
{
    public partial class UpdateemployeePhoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("Update Employees set PhotoPath = 'images/' + Cast(EmployeeId as varchar(5))+ '.png'");
            migrationBuilder.Sql("Update Employees set PhotoPath = 'images/nophoto.png' where EmployeeId not in (2,3,4,5,7,8)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {


        }
    }
}
