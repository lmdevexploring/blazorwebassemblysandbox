using BlazorProject.Server.Models;
using BlazorProject.Shared;
using Microsoft.AspNetCore.OData;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.OData.Edm;
using Microsoft.OData.ModelBuilder;

static IEdmModel GetEdmModelsForOdata()
{
    ODataConventionModelBuilder builder = new();
    builder.EntitySet<Employee>("EmployeesOData");
    return builder.GetEdmModel();
}


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

builder.Services.AddDbContext<AppDbContext>(options =>
     options.UseSqlServer(builder.Configuration.GetConnectionString("DBConnection"))
     .EnableSensitiveDataLogging());

builder.Services.AddControllers();
builder.Services.AddControllers().AddOData(options => options.Select().Filter().OrderBy().Expand().SkipToken()
                .AddRouteComponents("api/OData", GetEdmModelsForOdata()).Filter().Select().Expand()
);
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("alpha", new() { Title = "Testing OData", Version = "alpha" });
    c.ResolveConflictingActions(apiDesc => apiDesc.First());

});

builder.Services.AddScoped<IDepartmentRepository, DepartmentRepository>();
builder.Services.AddScoped<IEmployeeRepository, EmployeeRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/alpha/swagger.json", "Testing OData alpha"));
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();


app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

app.Run();
