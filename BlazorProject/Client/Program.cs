using BlazorProject.Client;
using BlazorProject.Client.Services;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Syncfusion.Blazor;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddHttpClient<IEmployeeService, EmployeeService>(client =>
{
    client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);
});
builder.Services.AddHttpClient<IDepartmentService, DepartmentService>(client =>
{
    client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);
});

//builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddSyncfusionBlazor();
builder.Services.AddScoped<EmployeeAdaptor>();

Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NTcxNjQ5QDMxMzkyZTM0MmUzMGh2NkJwa2pZS1A4bDl4amdoNVBGbVZlbjNHcDRGRm5iRlJkYkZqQ0owdEE9;NTcxNjUwQDMxMzkyZTM0MmUzMGIzVFZHcVVjOHZrbWlUSE42N21TdVIyNloxQjRrQlVpWVgzRTFZSFlEVUE9;NTcxNjUxQDMxMzkyZTM0MmUzMEd6d0FMZC9sc0tXQ0hjdkdMVC9rM01pYnRGL29SOWt6TnZNenZGOXhkMnc9;NTcxNjUyQDMxMzkyZTM0MmUzMFUyc1BrQ3Q2M1V5OVlNaGdFUVNTQm5CbmhRcWszc2NxTitaV3ZUTHRRWms9;NTcxNjUzQDMxMzkyZTM0MmUzMGtWaDNpWW8vYVB4MCtEN05zOFVjV2drZ1ZiaXFSSGhmcjRzVEIrTDJ6RnM9;NTcxNjU0QDMxMzkyZTM0MmUzMGl2dFdJOWExUXA5RkJlRzZqa3VoSEY4OUFHbC8vUVF4bDVabGRHUlBPN1U9;NTcxNjU1QDMxMzkyZTM0MmUzME02eFFzSFV6SW5aNXhqRWxnaGxsTlpkZi9uRXI1VzNOZjRVbWJkV2Y4QzA9;NTcxNjU2QDMxMzkyZTM0MmUzME55Rm1nUjF2Q3BsRHJGaU4xRjFoVDdBTDd5REV6UjhlYVFiU2xTbUs2Yzg9;NTcxNjU3QDMxMzkyZTM0MmUzMG5HUDZJQVV4NmFjK0VvTDlIVi85dkRWOXlrMmFWMFVSejg5TCsvZGxiVGc9;NTcxNjU4QDMxMzkyZTM0MmUzMEo1a2RvTFNRWXR3WGM2ZldKeE1taDNRd05BVWxjdHFWWEtscVNBbUJJbzA9;NTcxNjU5QDMxMzkyZTM0MmUzMEx2NmZJajFWcmFLb2MrVTNMaFVwVS9wQkhrUFN0cE1wYlYwYitUWnRaeHM9");

await builder.Build().RunAsync();
