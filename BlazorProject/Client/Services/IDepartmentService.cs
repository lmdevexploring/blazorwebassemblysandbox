﻿using BlazorProject.Shared;

public interface IDepartmentService
{
    Task<IEnumerable<Department>> GetAllDepartments();
    Task<Department> GetDepartment(int departmentId);
}