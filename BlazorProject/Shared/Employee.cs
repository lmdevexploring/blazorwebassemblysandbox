﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlazorProject.Shared
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "FirstName must contains at least 2 characters")]
        public string? FirstName { get; set; }
        [Required]
        public string? LastName { get; set; }
        [EmailAddress]
        [Display(Name = "Gmail")]
        //[AllowedEmailDomain("gmail.com")]
        [RegularExpression(@"@(tst|gmail|yahoo|live)\.com$", ErrorMessage = "Invalid domain in email address. The domain must be tst.com, gmail.com, yahoo.com or live.com")]
        public string? Email { get; set; }
        
        [Editable(false)]
        [DisplayFormat(DataFormatString = "d")]
        [Display(Name = "DOB")]
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public int DepartmentId { get; set; }
        public string? PhotoPath { get; set; }
        public Department? Department { get; set; }
    }
}